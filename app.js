let padlock_svg_string = `<svg width="20px" height="20px" viewBox="0 0 180 220">
      <g id="g35" transform="matrix(0.33072917,0,0,-0.33072917,32.054369,70.329497)">
          <path
             inkscape:connector-curvature="0"
             style="fill:#aab8c2"
             d="m 207.21837,55.394929 c -52.474,0 -109.392226,-37.846293 -109.670004,-83.866 -0.41929,-69.463965 -0.0373,-101.302379 -0.0373,-101.302379 l 43.991204,1.91992 c 0,0 0,0 -0.0929,87.413459 -0.0293,27.615056 34.327,50.3239997 65.809,50.3239997 37.706,0 68.267,-30.5719997 68.267,-68.2669997 V -172.16107 h 45.511 v 113.777999 c 0,62.8399997 -50.938,113.778 -113.778,113.778"
             class="padlock-element"
             sodipodi:nodetypes="csccsssccsc" />
          <path
             inkscape:connector-curvature="0"
             style="fill:#ffac33"
             d="m 355.12937,-308.68307 c 0,-25.134 -20.378,-45.511 -45.511,-45.511 h -204.8 c -25.134004,0 -45.511004,20.378 -45.511004,45.511 v 136.533 c 0,25.134 20.378,45.511 45.511004,45.511 h 204.8 c 25.133,0 45.511,-20.378 45.511,-45.511 z"
             id="path29" />
      </g>
  </svg>`;

let padlock_svg_string2 = '<h1>iks de</h1>';

$(document).ready(function() {
  let lockable_inputs = $('.padlock-lockable');

  $.each(lockable_inputs, function(index, input){
    if($(input).prop('disabled')) {
      //adding lock to each input
      $(input).after(padlock_svg_string);
      $(input).next().find('.padlock-element').addClass('padlock-element-closing');
    } else {
      $(input).after(padlock_svg_string);
      $(input).next().find('.padlock-element').addClass('padlock-element-opening');
    }
  });

  $('.btn').click(function(){
    $.each($('.padlock-element'), function(index, element) {
      $(element).toggleClass('padlock-element-opening padlock-element-closing');
    });
  });
});
